pub const PREFIX: &str = "[¡R!] ";

macro_rules! color_println {
    (prefix=$xprefix:expr) => {
        println!("{}{}", crate::log::PREFIX, $xprefix)
    };
    (prefix=$xprefix:expr, $($arg:tt)*) => {
        println!("{}{}{}", crate::log::PREFIX, $xprefix, format!($($arg)*))
    };
}

#[macro_export]
macro_rules! log_debug {
    () => {
        color_println!(prefix="  ")
    };
    ($($arg:tt)*) => {
        color_println!(prefix="  ", $($arg)*)
    };
}

#[macro_export]
macro_rules! log_info {
    () => {
        color_println!(prefix="")
    };
    ($($arg:tt)*) => {
        color_println!(prefix="", $($arg)*)
    };
}

#[macro_export]
macro_rules! log_success {
    () => {
        color_println!(prefix="✨ ")
    };
    ($($arg:tt)*) => {
        color_println!(prefix="✨ ", $($arg)*)
    };
}

#[macro_export]
macro_rules! log_error {
    () => {
        color_println!(prefix="🔥 ")
    };
    ($($arg:tt)*) => {
        color_println!(prefix="🔥 ", $($arg)*)
    };
}

#[macro_export]
macro_rules! log_warn {
    () => {
        color_println!(prefix="")
    };
    ($($arg:tt)*) => {
        color_println!(prefix="", $($arg)*)
    };
}
