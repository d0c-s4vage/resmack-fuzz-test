
# Resmack Fuzz Test

This project is an exploration of a feedback-driven fuzzer that uses:

* ptrace for instrumentation
    * with dynamic memory breakpoints specified by the target process
* process snapshotting/restoring
    * using `/proc/pid/map`, `/proc/pid/clear_refs`, and `/proc/pid/pagemap`
    * using `process_vm_writev`
* performance counters as a feedback mechanism
* corpus management

Successful code and concepts implemented here will be absorbed into
[resmack](https://gitlab.com/d0c-s4vage/resmack) to make it a full fuzzer
instead of only grammar-based data generator.

## Features/Experiments

See the blog post [Resmack: Fuzzing Thoughts - Part 2](https://narly.me/posts/resmack-detour-full-fuzzer-experiment/)
for a full writeup on what is done in this project.

## Usage

[![running this project](resmack_fuzz_test.gif)](resmack_fuzz_test.gif)

### Dependencies

This project requires:

* Rust
* Linux
* `linux-tools-common`
* `linux-tools-generic`
* `linux-tools-$(uname -r)`
* `gcc`

A `perf_event_paranoid` level of `<= 2`:

```
echo 2 | sudo tee /proc/sys/kernel/perf_event_paranoid
```

### Building

Building the test target:

```bash
gcc target.c -o a.out
```

Building the fuzzer

```bash
cargo build --release
```

Running the fuzzer

```bash
cargo run --release ./a.out
```

All together now ♪ ♫

```bash
gcc target.c -o a.out && cargo run --release ./a.out AAAAAAAAA
```

### Running Tests

```bash
cargo test
```

### Running Benchmarks

```bash
cargo bench --bench snapshots -- --verbose
```
